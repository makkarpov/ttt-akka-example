Клиент
======

В этом задании вам необходимо реализовать клиент для игры в "Крестики-нолики". В другом задании вам необходимо реализовать сервер для этой же игры. Для тестирования клиента рекомендуется использовать как свой сервер, так и сервер своих коллег по группе.

Целью игры является составление линии из трех фигур одного типа на поле 3х3. Один игрок выставляет "крестики", другой игрок - "нолики". Допустимы горизонтальные, вертикальные или диагональные линии. Игра заканчивается победой одного из игроков или ситуацией ничьи.

Сервер контролирует несколько параллельно идущих игр. Клиенты обрабатывают ходы пользователей. Пользователи играют друг с другом в парах при помощи клиентов. Вначале  случайным образом выбираются два игрока и они совершают ходы до возникновения финальной ситуации (выигрыша одного из игроков или ничьи) или прерывания игры одним из игроков.

Запуск клиента
--------------

Клиент должен поддерживать следующий формат командной строки: `СИМВОЛ ИМЯ`, где `СИМВОЛ` - это одна из букв `X`, `x`, `O`, `o`, означающих фигуру игрока, `ИМЯ` - это последовательность латинских букв, означающих имя игрока (размер имени ограничен только возможностями командной строки и может отличаться в разных ОС).

Сразу после запуска клиент выполняет инициализацию. Цель инициализации - получение информации об игре и о сопернике (игроке противоположной фигуры).

Клиент подключается к серверу при помощи сокета по имени файла `/tmp/ttt` (сокет во внутреннем домене потокового типа, протокол выбирается автоматически). Клиент посылает на сервер (записывает в сокет) имя своего пользователя (целое число типа `int` с длиной строки и последовательность байтов строки без завершающего нуля) и его фигуру (1 байт - `'X'` или `'O'`). Затем клиент ждет, пока сервер поместит в сокет имя пользователя-соперника (целое число типа `int` с длиной строки и последовательность байтов в количестве длины строки) и идентификаторы IPC-объектов игры в том порядке, в котором они перечислены в следующем абзаце. Если не удалось подключиться к серверу, отправить или считать данные, клиент пишет строку с причиной ошибки, полученной при помощи функций `strerror` или `perror`, и завершается с кодом 1.

Далее клиент подключается к следующим существующим IPC-объектам, используя идентификаторы, полученные от сервера:

1. разделяемая память размера 3х массивов из 3х целых чисел тип `int` (для игрового поля; строка массива соответствует строке игрового поля; элемент массива равен 1, если в поле стоит крестик, -1, если стоит нолик, 0, если в поле нет фигуры);
2. массив семафоров из 1 семафора (для синхронизации доступа к игровому полю; семафор равен 0 - поле не заблокировано для операции; семафор равен 1 - поле заблокировано; начальное значение семафора равно 0);
3. очередь сообщений (для обмена информацией о ходах между клиентами во время игры).

Процесс игры
------------

Начинают крестики.

Клиент работает в следующем цикле с пользователем. Клиент пишет приглашение для ввода команды пользователю, пользователь пишет команду на стандартный ввод клиенту, клиент выполняет команду. Если команда не меняет состояние игрового поля, клиент пишет приглашение и запрашивает следующую команду, а в противном случае клиент проверяет наличие финальной ситуации, отправляет клиенту соперника сделанный ход или признак окончания игры (см. ниже), получает от него ход, сделанный соперником (если игра не закончилась), и пишет его на стандартный вывод (формат описан ниже).

Приглашение состоит из имени пользователя, двоеточия и 1 символа пробела.

Клиент принимает 2 команды (ввод, не являющийся одной из этих команд, игнорируется, клиент пишет приглашение для ввода следующей команды):

* команда для совершения хода. Чтобы дать эту команду, пользователь пишет только координату клетки, т.е. только два числа от 1 до 3, разделенных пробелами, табуляциями, символами перевода строки и т.п. Первым идет номер строки игрового поля, вторым - номер столбца. Клиент проверяет, свободна ли указанная клетка. Если она занята, клиент пишет на стандартный вывод сообщение `Occupied` и пишет приглашение для ввода следующей команды. Если клетка свободна, клиент меняет ее значение.
* команда для печати текущего состояния игры. Чтобы дать эту команду, пользователь пишет число 0. Клиент печатает 3 строки, в каждой строке только 3 символа. Первая строка - это первая строка поля, вторая - вторая, третья - третья. Крестику соответствует символ Х, нолику - символ О, отсутствию знака - символ точка.

Поскольку оба клиента работают параллельно с одной разделяемой памятью, необходимо синхронизировать их работу с разделяемой памятью. А именно, доступ к разделяемой памяти должен быть помещен в "семафорные скобки". Иными словами, только один из процессов может в один момент времени работать с разделяемой памятью.

Выполнив команду, изменившую состояние игрового поля, клиент должен проверить наличие финальной ситуации.

Если после хода игрока реализовалась финальная ситуация, клиент пишет на стандартный вывод одно из сообщений `You win!`, `You lose`, `We tied`, отправляет клиенту соперника два целых числа типа `int`, равных нулю (это признак окончания игры), закрывает сокет с сервером и завершается с кодом 0.

Если же не реализовалась финальная ситуация, клиент посылает сделанный ход клиенту соперника (два целых числа типа `int`) и ожидает от него ответный ход. В момент ожидания клиент не реагирует на ввод. Ответом будет координата точки, куда соперник сделал ход (два целых числа типа `int`), или два целых числа типа `int`, равных нулю, если игра окончена. Если ход соперника получен (а не нули), клиент пишет на стандартный вывод строку из имени противника, двоеточия, пробела и хода, сделанного противником (два целых числа через пробел), и принимает следующую команду игрока. Если получен признак окончания игры, клиент сообщает о финальной ситуации (как, написано в предыдущем абзаце) и завершается с кодом 0.

Пользователь в любой момент может прервать клиент нажатием клавиш `Ctrl+C`. В этом случае клиент ничего не печатает на стандартный вывод, разрывает сокет с сервером и  завершается с кодом 1. Аналогичная реакция клиента должна быть в том случае, когда он обнаруживает окончание ввода перед очередным чтением ввода.

Сервер может в любой момент завершиться. Клиент узнает об этом при попытке воспользоваться IPC-объектом, который будет в этот момент уже удален. В этом случае клиент печатает на стандартный вывод сообщение `Server disconnected` и завершается с кодом 1.

Ограничения реализации
----------------------

В коде клиента запрещается использовать функции `system`, `exec***`. При корректном завершении процессы должны освобождать занимаемые ими ресурсы (файлы, динамическую память и т.п.). Можно предполагать, что клиент запускается в отдельной группе процессов.

Сервер
======

В этом задании вам необходимо реализовать сервер для игры в "Крестики-нолики". В другом задании вам необходимо реализовать клиент для этой же игры. Для тестирования сервера рекомендуется использовать как свой клиент, так и клиент своих коллег по группе.

Целью игры является составление линии из трех фигур одного типа на поле 3х3. Один игрок выставляет "крестики", другой игрок - "нолики". Допустимы горизонтальные, вертикальные или диагональные линии. Игра заканчивается победой одного из игроков или ситуацией ничьи.

Сервер контролирует несколько параллельно идущих игр. Его задача - принимать запросы клиентов на игру, искать партнеров, создавать IPC-объекты перед их игрой и удалять их после окончания игры.

В начале своей работы сервер открывает слушающий сокет по имени файла `/tmp/ttt` (сокет во внутреннем домене потокового типа, протокол выбирается автоматически) и принимает запросы на подключение к нему от клиентов. Приняв запрос, сервер считывает имя пользователя клиента и его фигуру (формат смотрите в описании клиента) и печатает на стандартный поток вывода строку вида `Connect: ФИГУРА ИМЯ`, где ФИГУРА - это символ Х, если клиент будет играть за крестиков, и О, если за ноликов, ИМЯ - это имя пользователя клиента.

Как только к серверу подключились два произвольных клиента с разными фигурами, которые еще не соединены в пару игроков, сервер создает IPC-объекты для их игры и, если это успешно, посылает каждому в их сокеты имена их соперника и идентификаторы IPC-объектов, а на стандартный поток вывода пишет строку вида `Start: ИМЯ-КРЕСТИКА ИМЯ-НОЛИКА` (после слова Start идет имя пользователя, играющего крестиками, затем идет 1 пробел, затем имя пользователя, играющего ноликами). Набор IPC-объектов игры перечислен в описании клиента.

Если не удалось создать хотя бы один из IPC-объектов, уже созданные IPC-объекты для данной игры должны быть удалены, причина ошибки создания должна быть написана на стандартный поток ошибок при помощи функций `strerror` или `perror`, сокеты с клиентами закрыты.

Сокет должен обрабатывать одновременно до 10 запросов от клиентов. Количество игр, которые параллельно может поддерживать сервер, ограничено только размерами системных таблиц ресурсов ядра ОС.

Как только оба сокета клиентов закрылись, сервер удаляет все IPC-объекты игры, пишет на стандартный вывод строку `Finish: ИМЯ-КРЕСТИКА ИМЯ-НОЛИКА` и считает игру оконченной.

В тот момент, когда клиент закрывает сокет с сервером (по запросу пользователя клиента или по окончании игры), сервер пишет строку `Disconnect: ИМЯ`, где ИМЯ - это имя пользователя клиента. При завершении игры сначала пишется строка Finish, а потом строки Disconnect.

Сервер должен реагировать на приход сигнала `SIGTERM`. В этом случае он удаляет все созданные IPC-объекты, разрывает все сокеты и завершается с кодом 0.

Сервер должен реагировать на приход сигнала `SIGINT`. В этом случае он распечатывает список текущих игр и список ожидающих клиентов. Список текущих игр должен состоять из строк, каждая строка должны быть парой имен через пробел: имя пользователя, играющего крестиками, затем пробел, затем имя пользователя, играющего ноликами. Последовательность строк в этом списке должна совпадать с последовательностью печати строк, начинающихся со Start. Список ожидающих клиентов тоже состоит из строк, каждая строка начинается с фигуры клиента (символ 'X' или 'O'), затем пробел, затем имя пользователя. Последовательность строк в этом списке должна совпадать с последовательностью печати строк, начинающихся с Connect. На время печати списка игр и клиентов начало и окончание игр, а также прием новых клиентов должны быть отложены.

В коде сервера запрещается использовать функции `system`, `exec***`. При корректном завершении процессы должны освобождать занимаемые ими ресурсы (файлы, динамическую память и т.п.). Можно предполагать, что сервер запускается в отдельной группе процессов. Отцовский процесс должен завершаться последним.