import java.io.{DataOutputStream, DataOutput, DataInputStream}
import java.net.{InetSocketAddress, Socket}
import java.nio.charset.StandardCharsets
import java.util.concurrent.locks.ReentrantLock

import scala.util.Try

// А теперь посмотрим, как сделать клиент без библиотеки akka (т.к. она весит мегабайт 10 и на клиент её тащить
// не хочется). Здесь мы сделаем клиент блокирующим
object Client extends App {
  // Объявим сперва те самые утилитарные классы, что и на сервере
  object PlayerType extends Enumeration {
    val None  = Value("none")
    val Cross = Value("cross")
    val Zero  = Value("zero")

    def opposite(v: Value) = v match {
      case Cross => Zero
      case Zero => Cross
    }

    def unapply(s: String): Option[Value] = Try(withName(s)).toOption
  }

  object GameResult extends Enumeration {
    val None  = Value("none")
    val Cross = Value("cross")
    val Zero  = Value("zero")
    val Tie   = Value("tie")

    def unapply(s: String): Option[Value] = Try(withName(s)).toOption
  }

  // Утилитарный объект, чтобы было удобнее координаты из строк извлекать
  object coord {
    def apply(x: Int, y: Int): Int = (y * 3) + x
    def unapply(s: String): Option[Int] = Try(s.toInt).filter(x => (x >= 1) && (x <= 3)).map(_ - 1).toOption
  }

  object fieldNum {
    def unapply(s: String): Option[Int] = Try(s.toInt).filter(x => (x >= 0) && (x < 9)).toOption
  }

  // Формат аргументов будет таким (argv[0] не учитывается):
  // 0 - IP-адрес сервера, куда надо присоединиться
  // 1 - крестик или нолик
  // 2 - имя игрока
  // Распарсим же его

  val (ip, playerType, localName) = args match {
    case Array(i, "x" | "X", n) => (i, PlayerType.Cross, n)
    case Array(i, "o" | "O" | "0", n) => (i, PlayerType.Zero, n)
    case _ =>
      Console.err.println("Bad arguments")
      Console.err.println("Format: [ip] [X or O] [name]")
      sys.exit(1)
  }

  // Соединяемся с сервером, создаем потоки и игровое поле
  val socket = new Socket(ip, 29592)
  val dataIn = new DataInputStream(socket.getInputStream)
  val dataOut = new DataOutputStream(socket.getOutputStream)
  val field  = Array.fill(9)(PlayerType.None)

  // Прием пакета от сервера в блокирующем режиме
  def recv: Seq[String] = (0 until dataIn.readInt()).map { _ =>
    val arr = new Array[Byte](dataIn.readInt())
    dataIn.readFully(arr)
    new String(arr, StandardCharsets.UTF_8)
  }

  // Отправка пакета серверу
  def send(strs: String*) = {
    dataOut.writeInt(strs.size)
    for (s <- strs; b = s.getBytes(StandardCharsets.UTF_8)) {
      dataOut.writeInt(b.length)
      dataOut.write(b)
    }
  }

  // Распечатка текущего состояния игрового поля
  def printField(): Unit = for (y <- 0 until 3) {
    for (x <- 0 until 3; f = field(coord(x, y))) print(f match {
      case PlayerType.None  => '.'
      case PlayerType.Zero  => 'O'
      case PlayerType.Cross => 'X'
    })
    println()
  }

  def makeMove(): Unit = {
    printField()
    while (true) {
      print(s"$localName: ")
      Console.in.readLine().split("[\t ]+") match {
        case Array(coord(y), coord(x)) => field(coord(x, y)) match {
          case PlayerType.None =>
            field(coord(x, y)) = playerType
            send("action", coord(x, y).toString)
            return

          case _ =>
            println("Occupied, try again")
        }

        case _ => println("Format: [row] [column] from 1 to 3")
      }
    }
  }

  def printRemote(who: PlayerType.Value, fieldNum: Int): Unit =
    println(s"$remoteName: ${fieldNum / 3 + 1} ${fieldNum % 3 + 1}")

  def waitRemote(): Unit = recv match {
    case Seq("action", PlayerType(who), fieldNum(num)) =>
      field(num) = who
      printRemote(who, num)

    case Seq("victory", PlayerType(who), fieldNum(num), GameResult(res)) =>
      field(num) = who
      if (who != playerType)
        printRemote(who, num)

      printField()
      res match {
        case GameResult.Cross => println("X wins!")
        case GameResult.Zero => println("O wins!")
        case GameResult.Tie => println("We tied!")
      }

      sys.exit(0)

    case Seq("error", msg) =>
      println(s"Received an error: $msg")
      sys.exit(2)
  }

  println("Awaiting for another player...")
  send("ident", playerType.toString, localName)
  val Seq("ident", remoteName) = recv
  println(s"Game started: $localName ($playerType) <--> (${PlayerType.opposite(playerType)} $remoteName")

  if (playerType == PlayerType.Cross)
    makeMove()

  while (true) {
    waitRemote()
    makeMove()
  }
}
