import java.net.InetSocketAddress
import java.nio.ByteOrder
import java.nio.charset.StandardCharsets

import akka.actor._
import akka.io.Tcp._
import akka.io.{IO, Tcp}
import akka.util.{ByteString, ByteStringBuilder}

import scala.collection.mutable
import scala.util.Try

import Messages._

// Иллюстрация, как выглядел бы сервер крестиков-ноликов на более высокоуровневом языке, чем си.
// Более производительный сервер, чем на си, замечу, из-за особенностей платформы.

// Актор - сущность, которая может принимать и отправлять сообщения. Между акторами нет разделяемого
// состояния, потому синхронизация тут не нужна, да и вредна. Подход, когда система строится на
// основе акторов, называется реактивным программированием, т.к. мы не ждем наступления события (как,
// например, системный вызов read ждет, пока в трубе чего-то появится), а реагируем на него. Это позволяет
// использовать малое количество потоков для обработки большого количества сообщений, т.к. потоки никогда
// не блокируются и всегда заняты делом.

// main() нашей программы - создаем систему акторов и единственный актор в ней - главный.
object ServerMain extends App { ActorSystem.create("ttt").actorOf(Props[ServerMain], "main") }

// Вспомогательные типы, которые понадобятся для взаимодействия
object Messages {
  object UserMessage { // Юзер что-то из сети прокукарекал
    def apply(x: Seq[String]): UserMessage = new UserMessage(x)
    def unapplySeq(x: UserMessage) = Seq.unapplySeq(x.msg) // Лишний раз Seq при сопоставлении не писать
  }

  object PlayerType extends Enumeration {
    val None  = Value("none")

    val Cross = Value("cross")
    val Zero  = Value("zero")

    def opposite(v: Value) = v match {
      case Cross => Zero
      case Zero => Cross
    }

    def unapply(s: String): Option[Value] = Try(withName(s)).toOption
  }

  object GameResult extends Enumeration {
    val None  = Value("none")
    val Cross = Value("cross")
    val Zero  = Value("zero")
    val Tie   = Value("tie")

    def unapply(s: String): Option[Value] = Try(withName(s)).toOption
  }

  class UserMessage(val msg: Seq[String])

  case class QueueMe(tpe: PlayerType.Value, name: String)
  case class DequeueMe(tpe: PlayerType.Value)
  case class QueueOk(remoteName: String)

  case class ConnectedClient(name: String, connection: ActorRef)
  case class RunningGame(cross: ConnectedClient, zero: ConnectedClient, game: ActorRef)

  case class ClientAction(who: PlayerType.Value, cell: Int)
  case class ClientVictory(fact: ClientAction, who: GameResult.Value)
  case class ClientDied(who: PlayerType.Value)
  case object GameFinished

  case object CloseAck extends Event
  case object PrintStats
}

class ServerMain extends Actor with ActorLogging {
  import context.system

  // Говорим, что хотим слушать указанный адрес и порт, 0.0.0.0 - слушаем весь интернет
  IO(Tcp) ! Bind(self, new InetSocketAddress("0.0.0.0", 29592))

  // Функция, обарабтывающая пришедшие сообщения
  val receive: Receive = {
    case Bound(localAddress) => // прошел бинд на localAddress
      log.info(s"Successfully bound to $localAddress")

      // Создаем очередь игроков
      context.actorOf(Props[UserQueue], "queue")

      // Создаем актор, читающий из консоли - ибо это, к сожалению, блокирующая операция
      context.actorOf(Props(classOf[ConsoleReader], self), "console")
    case Connected(remote, local) => // кто-то к нам присоединился
      // Отвечаем, что хотим его обрабатывать свежесозданным актором
      sender() ! Register(context.actorOf(Props(classOf[UserConnection], sender())))

    case PrintStats =>
      context.actorSelection("queue") ! PrintStats
  }
}

class ConsoleReader(master: ActorRef) extends Actor {
  // Запускаем чтение первой строки
  override def preStart = { self ! Nil }

  val receive: Receive = {
    case Nil =>
      Console.in.readLine()
      master ! PrintStats
      self ! Nil // перезапускаем чтение следующей строки
  }
}

class UserQueue extends Actor with ActorLogging {
  var xQueue = mutable.Queue.empty[ConnectedClient]
  var oQueue = mutable.Queue.empty[ConnectedClient]
  var runningGames = Vector.empty[RunningGame]

  val receive: Actor.Receive = {
    case QueueMe(who, name) =>
      log.info(s"Queuing $name for $who")

      // Ставим в очередь
      val entry = ConnectedClient(name, sender())
      if (who == PlayerType.Cross)
        xQueue += entry
      else
        oQueue += entry

      // Если есть, кого соединять - соединяем
      if (xQueue.nonEmpty && oQueue.nonEmpty) {
        val cross = xQueue.dequeue()
        val zero  = oQueue.dequeue()

        log.info(s"Establishing session ${cross.name} (X) <--> (O) ${zero.name}")

        // Новый актор, который будет обрабатывать саму игру
        val game = context.actorOf(Props(classOf[UserGame], self, cross, zero))
        runningGames :+= RunningGame(cross, zero, game)
      }

    case DequeueMe(tpe) =>
      // Убираем из очереди пославшего это.
      if (tpe == PlayerType.Cross)
        xQueue = xQueue.filterNot(_.connection == sender())
      else
        oQueue = oQueue.filterNot(_.connection == sender())

    case GameFinished =>
      runningGames = runningGames.filter(_.game != sender())

    case PrintStats =>
      log.info("Game statistics: ")
      log.info("Runing games: ")

      for (g <- runningGames)
        log.info(s"  ${g.cross.name} (X) <--> (O) ${g.zero.name} (game: ${g.game})")

      log.info("X Queue: ")
      for (q <- xQueue)
        log.info(s"  ${q.name} (conn: ${q.connection})")

      log.info("O Queue: ")
      for (q <- oQueue)
        log.info(s"  ${q.name} (conn: ${q.connection})")
  }
}

class UserGame(queue: ActorRef, cross: ConnectedClient, zero: ConnectedClient) extends Actor with ActorLogging {
  override def preStart = {
    cross.connection ! QueueOk(zero.name)
    zero.connection ! QueueOk(cross.name)
  }

  // Нумерация клеток:
  //   /-----\
  //   |0 1 2|
  //   |3 4 5|
  //   |6 7 8|
  //   \-----/
  //
  // Выигрышные ряды
  val Rows = Seq(
    (0, 1, 2), (3, 4, 5), (6, 7, 8), // Горизонтали
    (0, 3, 6), (1, 4, 7), (2, 5, 8), // Вертикали
    (0, 4, 8), (2, 4, 6)             // Диагонали
  )

  var move = PlayerType.Cross
  val field = Array.fill(9)(PlayerType.None)

  def checkState =
    Rows.collect {
      // Ищем последовательности из трех одинаковых не пустых клетов
      case (i, j, k) if field(i) == field(j) && field(j) == field(k) && field(k) != PlayerType.None =>
        field(i) // и вытаскиваем из неё знак
    } match { // Предполагаем, что два выигрыша одновременно быть не может.
      case PlayerType.Cross :: _ => GameResult.Cross // Если первый элемент - крестик, то выиграли крестики
      case PlayerType.Zero :: _ => GameResult.Zero
      case Nil if field.contains(PlayerType.None) => GameResult.None // Если список пуст, но есть пустые клетки - пока играем
      case _ => GameResult.Tie // Пустых клеток нет, выигрыша нет - ничья
    }

  val receive: Receive = {
    case a @ ClientAction(who, num) =>
      if ((who == move) && (field(num) == PlayerType.None)) {
        field(num) = who

        checkState match {
          case GameResult.None => // Играем дальше
            move = PlayerType.opposite(move)
            (who match {
              case PlayerType.Cross => zero.connection
              case PlayerType.Zero => cross.connection
            }) ! a

          case st => // Финальная ситуация
            move = PlayerType.None // Ходить больше нельзя

            log.info(s"${cross.name} (X) <--> (O) ${zero.name}: Game result is $st")

            // Посылаем обоим клиентам информацию о финальном ходе и победе
            val pck = ClientVictory(ClientAction(who, num), st)
            zero.connection ! pck
            cross.connection ! pck

            // Завершаемся
            queue ! GameFinished
            context.stop(self)
        }
      }

    case ClientDied(who) =>
      if (who == PlayerType.Cross)
        zero.connection ! ClientDied
      else
        cross.connection ! ClientDied

      move = PlayerType.None
      queue ! GameFinished
      context.stop(self)
  }
}

class UserConnection(socket: ActorRef) extends Actor with ActorLogging {
  implicit val usedByteOrder = ByteOrder.BIG_ENDIAN
  var buffer: ByteString = ByteString.empty

  override def postStop = {
    context.stop(socket)
  }

  def parse(): Boolean = { // Это такое объявление функции, слегка похоже на то, что в паскале (по типам - точно)
    try {
      val stream = buffer.iterator

      // Вот здесь мы можем читать данные, думая, что система синхронная, хотя это не так -- нигде
      // тут не происходит блокировки и мы можем обрабатывать сразу кучу данных в нескольких потоках,
      // но асинхронность абстрагирована replaying-логикой. Если вдруг мы попытаемся считать того, чего
      // еще нет - вылетит NoSuchElementException и чтение будет повторено при поступлении новых данных.
      // Как только данных будет достаточно для полного чтения - мы запишем в буффер остаток и попробуем
      // снова - вдруг пришло два сообщения в одном пакете.

      // Теперь перейдем к протоколу. Для простоты положим, что его формат такой - сначала идет инт - кол-во записей,
      // потом идет N записей, каждая из которых состоит из инта длины и потом N байт строки в кодировке UTF8.
      // Протокол, таким образом, получается независимый от состояния -- там всегда гоняются строки, а не "здесь
      // char, здесь int, а тут рыбу заворачивали". Состояние мы разберем потом при помощи сопоставления с шаблонами.
      // (вариант с заворачиваемой рыбой тоже допустим, но когда перед каждым пакетом есть какой-то маркер, что этот
      //  пакет обозначает. В таком случае мы читаем маркер, находим класс пакета и разбираем поток в соответствии с
      //  его структурой. Протокол, таким образом, тоже получается независимый от состояния, но более эффективный,
      //  чем гонять сырые строки туда-сюда.)

      // Отображаем множество чисел [0, длина) в считанные строки
      val parsed = (0 until stream.getInt).map(_ => stream.getByteString(stream.getInt).utf8String)

      // Отправляем итог самим себе, чтобы потом обработать
      self ! Messages.UserMessage(parsed)

      buffer = stream.toByteString // Остаток данных возвращаем в буфер
      true // возвращаем, что разбор удался
    } catch {
      case e: NoSuchElementException =>
        // Не хватает данных. Тихо сжираем эту ошибку, они еще появятся.
        false // разбор не удался
    }
  }

  def send(strings: String*)(ack: Event = NoAck): Unit = { // X* - бесконечное число параметров типа X, Unit == void
    val bsb = new ByteStringBuilder

    bsb.putInt(strings.length)
    for (s <- strings; b = s.getBytes(StandardCharsets.UTF_8)) {
      bsb.putInt(b.length)
      bsb.putBytes(b)
    }

    socket ! Write(bsb.result(), ack)
  }

  // Актор может находиться в одном из нескольких состояний:

  // Выполняется всегда
  val baseReceive: Receive = {
    case Received(data) =>
      buffer ++= data // Приписываем в конец принятые данные
      while (parse()){} // Пока можно что-то напарсить - делаем.
  }

  // Встали в очередь
  def queuedReceive(name: String, mode: PlayerType.Value): Receive = {
    case QueueOk(rname) =>
      // Нас с кем-то соединили, переключаемся на игровое состояние и отправляем его игроку

      context.become(gameReceive(name, mode, sender()) orElse baseReceive)
      send("ident", rname)()

    case PeerClosed =>
      // Если коннект в это время исчез -- то выходим из очереди и останавливаемся

      context.actorSelection("/user/main/queue") ! DequeueMe(mode)
      context.stop(self)

      log.info(s"Player '$name' disconnecting while staying in queue")
  }

  // Играем
  def gameReceive(name: String, mode: PlayerType.Value, game: ActorRef): Receive = {
    case UserMessage("action", num) =>
      // Хитрая монада, которая превращается в Error, если не получилось разобрать число или оно вне диапазона.
      // Такие ошибки тихо игнорируются

      Try(num.toInt).filter(x => x >= 0 && x < 9).foreach(game ! ClientAction(mode, _))

    // отправляем изменение хода юзеру
    case ClientAction(who, num) =>
      send("action", who.toString, num.toString)()

    case ClientVictory(ClientAction(who, num), result) =>
      // После пакета победы коннект обычно закрывается, но мы ему в этом поможем. Т.к. оно по жопу асинхронное,
      // нет особых гарантий, что сначала будет Write, а потом Close. Эту гарантию надо ввести явно при помощи
      // Ack-пакета, который будет отправлен, когда Write завершится. Мы его обработаем в ветке ниже и закроем
      // коннект уже явно

      send("victory", who.toString, num.toString, result.toString)(CloseAck)

    case ClientDied =>
      // противник сдох
      send("error", "remote client died")(CloseAck)

    case CloseAck =>
      socket ! Close
      log.info(s"Disconnecting player '$name'")
      context.stop(self)

    case PeerClosed =>
      game ! ClientDied(mode)
      log.info(s"Player '$name' left the game")
  }

  // Состояние по умолчанию, оно же -- только присоединились к серверу.
  val receive: Receive = baseReceive orElse {
    case UserMessage("ident", PlayerType(tpe), name) =>
      // встанем в очередь обработки и сменим трактовку сообщений в связи с изменившимся состоянием
      context.become(queuedReceive(name, tpe) orElse baseReceive)
      context.actorSelection("/user/main/queue") ! QueueMe(tpe, name)

    case PeerClosed =>
      // пока ничего нет - просто завершаемся и сами тоже вслед за сокетом
      context.stop(self)
  }
}